importScripts('cache-polyfill.js');

// Este service worker cachea todos los recursos para no generar trafico en la red y convierte a la PWA en instalable.
self.addEventListener('install', function(e) {
  e.waitUntil(
    caches.open('NYSL-Dreamcatcher').then(function(cache) {
      return cache.addAll([
        'android-chrome-192x192.png',
        'android-chrome-512x512.png',
        'apple-touch-icon.png',
        'favicon-16x16.png',
        'favicon-32x32.png',
        'favicon.ico',
        'mstile-150x150.png',
        'safari-pinned-tab.svg',
        'manifest.json',
        'img/School1.5c812ea6.jpg',
        'img/School2.14f2cc29.jpg',
        'img/School3.115e7eb5.jpg',
        'img/School4.6bb3acda.jpg',
        'img/School5.0b9184c8.jpg',
        'img/School6.78031d4a.jpg',
        'js/app.a55f4d01.js',
        'js/app.a55f4d01.js.map',
        'js/chunk-vendors.2bbb2d22.js',
        'js/chunk-vendors.2bbb2d22.js.map',
        'index.html',



      ]);
    })
  );
});
self.addEventListener('fetch', function(event) {
  console.log(event.request.url);
  event.respondWith(
    // si la info esta cacheada, muestra eso, caso contrario realiza el fetch al server.
    caches.match(event.request).then(function(response) {
      return response || fetch(event.request);
    })
  );
});