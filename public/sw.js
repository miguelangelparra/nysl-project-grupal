importScripts('cache-polyfill.js');

// Este service worker cachea todos los recursos para no generar trafico en la red y convierte a la PWA en instalable.
self.addEventListener('install', function(e) {
  e.waitUntil(
    caches.open('NYSL-Dreamcatcher').then(function(cache) {
      return cache.addAll([

      ]);
    })
  );
});
self.addEventListener('fetch', function(event) {
  console.log(event.request.url);
  event.respondWith(
    // si la info esta cacheada, muestra eso, caso contrario realiza el fetch al server.
    caches.match(event.request).then(function(response) {
      return response || fetch(event.request);
    })
  );
});