//imports de vue
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//imports de estilos
//import 'materialize-css'
import 'materialize-css/dist/css/materialize.min.css'
import 'materialize-css/dist/js/materialize.min.js'
import './assets/styles/styles.css'
//Imports de firebase
import firebase from 'firebase'
import 'firebase/firebase-app.js'
require('firebase/app')
require('firebase/database')
require('firebase/auth')

//Configuracion de firebase
const firebaseConfig = {
  apiKey: "AIzaSyBWNk9mp2URewAKPRB01L-x2v5ILL66XI8",
  authDomain: "nysl-dreamcatcher.firebaseapp.com",
  databaseURL: "https://nysl-dreamcatcher.firebaseio.com",
  projectId: "nysl-dreamcatcher",
  storageBucket: "",
  messagingSenderId: "471523521197",
  appId: "1:471523521197:web:38cad74463638e20"
};
// Initializacion de  Firebase
firebase.initializeApp(firebaseConfig);

//Asuntos propios de vue
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')


