//Imports de Vue
import Vue from 'vue'
import Router from 'vue-router'
//Imports internas
import Home from './views/Home.vue'
import Schedule from './views/Schedule.vue'
import Stadiums from './views/Stadiums.vue'
import Filters from './views/Filters.vue'
import ChatModal from './components/ChatModal.vue'
//Imports internas relacionadas con Firebase
import { filter } from 'minimatch';



Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [

    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/schedule',
      name: 'schedule',
      component: Schedule,
    },

    {
      path: '/stadiums',
      name: 'stadiums',
      component: Stadiums,
    },

    {
      path: '/filters',
      name: 'filters',
      component: Filters,
    },
    {
      path: '/ChatModal/:id',
      name: 'ChatModal',
      component: ChatModal,
    },
  ]
})